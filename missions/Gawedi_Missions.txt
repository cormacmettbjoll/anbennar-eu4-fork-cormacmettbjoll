gawed_lencenor = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = A13
	}
	has_country_shield = yes
	
	gawed_wars_of_dominion = {
		icon = mission_unite_home_region
		required_missions = { }
		#position = 1
		
		trigger = {
				if = {
					limit = {
						exists = A01
					}
					war_with = A01
			}
		}
		
		effect = {
			small_country_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "army_enthusiasm"
				duration = 7300
			}
		}
	}
	
	gawed_halfling_suzerainty = {
		icon = mission_monarch_in_throne_room
		required_missions = { gawed_wars_of_dominion }
		#position = 2
		
		provinces_to_highlight = {
			region = small_country_region
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			small_country_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}

		effect = {
			small_country_region = {
				add_province_modifier = {
					name = "faster_integration"
					duration = 7300
				}
			}
			redglades_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	gawed_burning_the_redglades = {
		icon = mission_cannons_firing
		required_missions = { gawed_halfling_suzerainty }
		#position = 4
		
		provinces_to_highlight = {
			area = redglades_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			redglades_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "generic_into_lorent"
				duration = 5475
			}
			
			country_event = { id = flavor_gawed.1 days = 1 }
		}
	
	}
	
	gawed_conquer_lorentaine = {
		icon = mission_cannons_firing
		required_missions = { gawed_burning_the_redglades }
		#position = 4
		
		provinces_to_highlight = {
			OR = {
				area = ionnidar_area
				area = upper_bloodwine_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			ionnidar_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			upper_bloodwine_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			67 = {
				owned_by = ROOT
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "imperial_ambition"
				duration = 5475
			}
		}
	}
}


gawed_imperial_expansion = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = A13
	}
	has_country_shield = yes
	
	gawed_raise_invasion_force = {
		icon = mission_assemble_an_army
		required_missions = { }
		position = 1
		
		trigger = {
			army_size_percentage = 1
			num_of_generals = 1
		}
		
		effect = {
			vertesk_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	gawed_invasion_of_vertesk = {
		icon = mission_cannons_firing
		required_missions = { gawed_raise_invasion_force }
		#position = 2

		provinces_to_highlight = {
			area = vertesk_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			vertesk_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			arbaran_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			derwing_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			golden_plains_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			cestir_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	gawed_invasion_of_arbaran = {
		icon = mission_have_two_subjects
		required_missions = { gawed_invasion_of_vertesk }
		#position = 2

		provinces_to_highlight = {
			OR = {
				area = arbaran_area
				area = derwing_area
				area = golden_plains_area
				area = cestir_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			arbaran_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			derwing_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			golden_plains_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			cestir_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			damescrown_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			908 = { 
				if = {
					limit = {
						NOT = {
							owned_by = ROOT
							is_core = ROOT
						}
					}
					add_permanent_claim = ROOT
				}
			}
			254 = { 
				if = {
					limit = {
						NOT = {
							owned_by = ROOT
							is_core = ROOT
						}
					}
					add_permanent_claim = ROOT
				}
			}
		}
	}
		
	gawed_secure_border_with_empire = {
		icon = mission_conquer_50_development
		required_missions = { gawed_invasion_of_arbaran }
		#position = 2

		provinces_to_highlight = {
			OR = {
				area = damescrown_area
				province_id = 908
				province_id = 254
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			arbaran_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			derwing_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			golden_plains_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			cestir_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			damescrown_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			heartland_borders_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
		
	}
}

gawed_internal = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = A13
	}
	has_country_shield = yes
	
	gawed_build_the_bulwark = {
		icon = mission_conquer_50_development
		required_missions = { }
		position = 1
		
		provinces_to_highlight = {
			OR = {
				province_id = 228
				province_id = 242
				province_id = 343
				province_id = 247
			}
		}
		
		trigger = {
			228 = {
				owned_by = ROOT
				has_fort_building_trigger = yes
				base_manpower = 3
			}
			242 = {
				owned_by = ROOT
				base_manpower = 3
			}
			343 = {
				owned_by = ROOT
				has_fort_building_trigger = yes
				base_manpower = 3
			}
			247 = {
				owned_by = ROOT
				base_manpower = 3
			}
		}
			
		effect = {
			228 = {
				add_permanent_province_modifier = {
					name = bulwark_fortifications
					duration = -1
				}
			}
			242 = {
				add_permanent_province_modifier = {
					name = bulwark_fortifications
					duration = -1
				}
			}
			343 = {
				add_permanent_province_modifier = {
					name = bulwark_fortifications
					duration = -1
				}
			}
			247 = {
				add_permanent_province_modifier = {
					name = bulwark_fortifications
					duration = -1
				}
			}
		}
	}
	
	gawed_occupy_balmire = {
		icon = mission_build_up_to_force_limit
		required_missions = { gawed_build_the_bulwark }
		#position = 2
		
		provinces_to_highlight = {
			area = balmire_area
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
		}
		
		trigger = {
			balmire_area = {
				type = all
				owned_by = ROOT
			}
		}

		effect = {
			middle_alen_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			athfork_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			westwall_approach_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = {
				name = "gawed_gawedi_settlers"
				duration = 3650
			}
		}
	}
	
	gawed_securing_the_alen = {
		icon = mission_unite_home_region
		required_missions = { gawed_occupy_balmire }
		#position = 4
		
		provinces_to_highlight = {
			OR = {
				area = middle_alen_area
				area = athfork_area
				area = westwall_approach_area
			}
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
		}
		
		trigger = {
			middle_alen_area = {
				type = all
				owned_by = ROOT
			}
			athfork_area = {
				type = all
				owned_by = ROOT
			}
			westwall_approach_area = {
				type = all
				owned_by = ROOT
			}
		}

		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 7300
			}
		}
	}
}


gawed_colonial = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = A13
	}
	has_country_shield = yes
	
	gawed_integrate_westmoors = {
		icon = mission_rb_control_electors
		required_missions = {  }
		position = 1
		
		trigger = {
			NOT = { exists = A24 }
			westmoor_proper_area = {
				type = all
				owned_by = ROOT
			}
			moorhills_area = {
				type = all
				owned_by = ROOT
			}
			beronmoor_area = {
				type = all
				owned_by = ROOT
			}
		}
			
		effect = {
			westmoor_proper_area = {
				add_province_modifier = {
					name = "faster_integration"
					duration = 7300
				}
			}
			moorhills_area = {
				add_province_modifier = {
					name = "faster_integration"
					duration = 7300
				}
			}
			beronmoor_area = {
				add_province_modifier = {
					name = "faster_integration"
					duration = 7300
				}
			}
		}
	}
	
	gawed_revitalize_western_ports = {
		icon = mission_galleys_in_port
		required_missions = { gawed_integrate_westmoors }
		#position = 2
		
		provinces_to_highlight = {
			OR = {
				province_id = 191
				province_id = 192
				province_id = 193
			}
		}
		
		trigger = {
			191 = {
				owned_by = ROOT
				OR = {
					has_trade_building_trigger = yes
					has_dock_building_trigger = yes
				}
			}
			192 = {
				owned_by = ROOT
				OR = {
					has_trade_building_trigger = yes
					has_dock_building_trigger = yes
				}
			}
			193 = {
				owned_by = ROOT
				OR = {
					has_trade_building_trigger = yes
					has_dock_building_trigger = yes
				}
			}
		}

		effect = {
			191 = {
				add_base_tax = 2
				add_base_production = 2
				add_base_manpower = 1
				add_center_of_trade_level = 1
			}
			192 = {
				add_base_tax = 1
				add_base_production = 2
				add_base_manpower = 1
			}
			193 = {
				add_base_tax = 1
				add_base_production = 2
				add_base_manpower = 1
			}
		}
	}

	gawed_rediscovery_of_aelantir = {
		icon = mission_sea_battles
		required_missions = { gawed_revitalize_western_ports }
		position = 5
		trigger = {
			custom_trigger_tooltip = {
				tooltip = aelantir_discovery.tooltip
				colonial_endralliande = {
					has_discovered = ROOT
				}
			}
		}

		effect = {
			add_country_modifier = {
				name = "colonial_enthusiasm"
				duration = 7300
			}
		}
	}
	
	gawed_colonise_endralliande = {
		icon = mission_colonial
		required_missions = { gawed_rediscovery_of_aelantir }
		
		provinces_to_highlight = {
			colonial_region = colonial_endralliande
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
			has_discovered = ROOT
		}
		
		trigger = { 
			custom_trigger_tooltip = {
				tooltip = miss_colonize_endralliande.tooltip
				num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 5
					colonial_region = colonial_endralliande
				}
			}
		}
	
		effect = {
			add_country_modifier = {
				name = "eng_rb_colony_chance"
				duration = 7300
			}
		}
	}
}


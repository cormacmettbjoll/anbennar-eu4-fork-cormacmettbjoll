# No previous file for Wudhal
owner = Z18
controller = Z18
add_core = Z18
culture = gray_orc
religion = old_dookan
capital = ""
trade_goods = fur

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

native_size = 27
native_ferocity = 8
native_hostileness = 8